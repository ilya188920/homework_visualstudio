#include <iostream>
#include <string>

using namespace std;


class Animal
{
public:
	 virtual void Voice()
	{
		
			cout << "Sound: ";
		
	}
};

class Cat : public Animal 
{
public:
	void Voice() override
	{
		
		cout << "Meow" << endl;
	}
};

class bird : public Animal
{
public:
	void Voice() override
	{
		
		cout << "chirp-chirp" << endl;
	}
};

class dog : public Animal
{
public:
	void Voice() override
	{
		
			cout << "Woof" << endl;
	
	}
};

int main()
{
	Animal* selection = new Animal;
	int a;
	string k = "";
	cout << "1 - Cat\n2 - bird\n3 - dog\n";
	cout << "Enter a number from 1 to 3" << endl;
	cin >> a;
	
	while (a > 3)
	{
		cout << "Wrong number" << endl;
		cin >> a;
	}
	selection->Voice();
		if (a == 1)
		{
			selection = new Cat;
			selection->Voice();
		}
		if (a == 2)
		{
			selection = new bird;
			selection->Voice();
		}
		if (a == 3)
		{
			selection = new dog;
			selection->Voice();
		}

	cout << "Would you like a list of all the sounds?(Yes/No)" << endl;
	cin >> k;

	if (k == "Yes" || k == "yes")
	{
		Animal* mass[3];
		mass[0] = new Cat;
		mass[1] = new bird;
		mass[2] = new dog;
		
		for (Animal* i : mass)
		{
			i->Voice();
		}
	}

	
}